package edu.ib.mobilesequenceanalyzer;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {
    private volatile File file;
    private ListView listview;
    private ListActivityAdapter listActivityAdapter;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        ArrayList<Mutation> dane = null;
        Bundle bundle = getIntent().getExtras();

        String sek1 = bundle.getString("sekwencja1");
        String sek2 = bundle.getString("sekwencja2");
        Algorithm algorithm = new Algorithm();
        try {
            dane = algorithm.dotplot(sek1, sek2);
        } catch (IOException e) {
            e.printStackTrace();
        }
        listview = findViewById(R.id.listView1);
        listActivityAdapter = new ListActivityAdapter(dane, ListActivity.this);
        System.out.println(dane);
        listview.setAdapter(listActivityAdapter);
    }

    public void btnReturnOnClick(View view) {
        Intent intent = new Intent(getApplicationContext(), AnalyzerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}