package edu.ib.mobilesequenceanalyzer;

public class Mutation {
    private String name;
    private String position;
    private String before;
    private String after;

    public Mutation(String name, String position, String before, String after) {
        this.name = name;
        this.position = position;
        this.before = before;
        this.after = after;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getBefore() {
        return before;
    }

    public void setBefore(String before) {
        this.before = before;
    }

    public String getAfter() {
        return after;
    }

    public void setAfter(String after) {
        this.after = after;
    }

    @Override
    public String toString() {
        return "Mutation{" +
                "name='" + name + '\'' +
                ", position='" + position + '\'' +
                ", before='" + before + '\'' +
                ", after='" + after + '\'' +
                '}';
    }
}
