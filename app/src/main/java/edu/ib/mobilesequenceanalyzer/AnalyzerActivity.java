package edu.ib.mobilesequenceanalyzer;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;

public class AnalyzerActivity extends AppCompatActivity {

    private volatile String chosenGene;
    private FileChooserFragment fragment;
    private Button buttonShowInfo;
    private volatile String sekwencja = null;
    private volatile String path;
    private final String response = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analyzer);

        FragmentManager fragmentManager = this.getSupportFragmentManager();
        this.fragment = (FileChooserFragment) fragmentManager.findFragmentById(R.id.fragment_fileChooser);

        this.buttonShowInfo = this.findViewById(R.id.btnAnalysis);

        this.buttonShowInfo.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                try {
                    goToGraph();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void goToGraph() throws Exception {
        path = this.fragment.getPath();
        EditText chosenGeneEt = findViewById(R.id.etUniProt);
        chosenGene = chosenGeneEt.getText().toString();
        System.out.println(chosenGene);
        getRequest(chosenGene);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void getRequest(String concatData) {
        Thread thread = new Thread(() -> {
            String requestURL = "https://www.ebi.ac.uk/proteins/api/proteins/" + concatData;
            URL url = null;

            try {
                url = new URL(requestURL);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }


            URLConnection connection = null;

            try {
                connection = url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }

            HttpURLConnection httpConnection = (HttpURLConnection) connection;

            httpConnection.setRequestProperty("Accept", "application/json");

            InputStream response = null;
            try {
                response = connection.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

            int responseCode = 0;
            try {
                responseCode = httpConnection.getResponseCode();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (responseCode != 200) {
                throw new RuntimeException("Response code was not 200. Detected response was " + responseCode);
            }

            String output;
            Reader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(response, StandardCharsets.UTF_8));
                StringBuilder builder = new StringBuilder();
                char[] buffer = new char[8192];
                int read = 0;
                while (true) {
                    try {
                        if (!((read = reader.read(buffer, 0, buffer.length)) > 0)) break;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    builder.append(buffer, 0, read);
                }
                output = builder.toString();
            } finally {
                if (reader != null) try {
                    reader.close();
                } catch (IOException logOrIgnore) {
                    logOrIgnore.printStackTrace();
                }
            }
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            System.out.println(output);

            JsonObject results = gson.fromJson(output, JsonObject.class);

            JsonObject jakos = results.getAsJsonObject("sequence");
            System.out.println(jakos.toString());
            JsonPrimitive sekw = jakos.get("sequence").getAsJsonPrimitive();
            System.out.println(sekw.getAsString());
            sekwencja = (sekw.getAsString());

            if (response == null) {
                Toast.makeText(this, "There is no such gene in database.", Toast.LENGTH_LONG).show();
            } else {
                Algorithm algorithm = new Algorithm();
                try {
                    BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path)));
                    String line;
                    String refSequence = null;
                    try {
                        int lineNb = 0;
                        StringBuilder sb = new StringBuilder();
                        while ((line = br.readLine()) != null) {
                            if (lineNb == 0) {

                            } else {
                                sb.append(line);
                            }
                            lineNb++;
                        }
                        refSequence = sb.toString();
                        br.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    Intent intent = new Intent(getApplicationContext(), ListActivity.class);
                    Bundle bundle = new Bundle();

                    bundle.putString("sekwencja1", sekwencja);
                    bundle.putString("sekwencja2", refSequence);

                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }


}