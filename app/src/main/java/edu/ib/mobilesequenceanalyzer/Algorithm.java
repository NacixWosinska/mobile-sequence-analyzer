package edu.ib.mobilesequenceanalyzer;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.io.IOException;
import java.util.ArrayList;

public class Algorithm {

    @RequiresApi(api = Build.VERSION_CODES.O)
    public ArrayList<Mutation> dotplot(String sequence, String refSequence) throws IOException {
        ArrayList<Mutation> mutations = new ArrayList<>();
        char[] sekwencja1 = sequence.toCharArray();
        char[] sekwencja2 = refSequence.toCharArray();


        System.out.println("Dlugość sekwencji 1:" + sekwencja1.length);
        System.out.println("Dlugość sekwencji 2:" + sekwencja2.length);
        //char[] sekwencja1 = "ATGTGCC".toCharArray();
        //char[] sekwencja2 = "ACTGGCC".toCharArray();
        int min = Math.min(sekwencja2.length, sekwencja1.length);
        System.out.println(sekwencja2);
        System.out.println(sekwencja1);
        StringBuilder sekw2zmieniana = new StringBuilder();
        sekw2zmieniana.append(refSequence);

        for (int i = 0; i < min; i += 1) {
            if (sekwencja2[i] != sekwencja1[i]) {

                System.out.println(sekwencja2[i]);

                if (i == min - 1) {
                    mutations.add(new Mutation("Substitution", String.valueOf(i + 1), Character.toString(sekwencja2[i]),
                            Character.toString(sekwencja1[i])));
                    System.out.println("SUBSTYTUCJA, " + (i + 1));

                    //} else if (i == min - 2) {

                } else {
                    if (sekwencja2[i + 1] == sekwencja1[i] &&
                            sekwencja2[i + 2] == sekwencja1[i + 1]) {

                        mutations.add(new Mutation("Deletion", String.valueOf(i + 1), Character.toString(sekwencja2[i]),
                                Character.toString(sekwencja1[i])));
                        sekw2zmieniana.deleteCharAt(i);
                        System.out.println("DELECJA, " + (i + 1));
                        sekwencja2 = sekw2zmieniana.toString().toCharArray();

                    } else if (sekwencja2[i] == sekwencja1[i + 1] && sekwencja2[i + 1] == sekwencja1[i + 2]) {
                        mutations.add(new Mutation("Insertion", String.valueOf(i + 1), Character.toString(sekwencja2[i]),
                                Character.toString(sekwencja1[i])));
                        sekw2zmieniana.insert(i, sekwencja1[i]);
                        sekwencja2 = sekw2zmieniana.toString().toCharArray();
                        System.out.println("INSERCJA, " + (i + 1));
                        System.out.println(sekwencja2);
                    } else {
                        mutations.add(new Mutation("Substitution", String.valueOf(i + 1), Character.toString(sekwencja2[i]),
                                Character.toString(sekwencja1[i])));
                        System.out.println("SUBSTYTUCJA, " + (i + 1));
                    }

                }

            }
        }
        return mutations;
    }

}
