package edu.ib.mobilesequenceanalyzer;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class ListActivityAdapter extends BaseAdapter implements ListAdapter {
    private final Context context;
    private ArrayList<Mutation> mutations;

        public ListActivityAdapter(ArrayList<Mutation> mutations,
                               Context context) {
        this.mutations = mutations;
        this.context = context;
    }


    public ArrayList<Mutation> getList() {
        System.out.println(Arrays.asList(mutations));
        return mutations;
    }

    @Override
    public int getCount() {
        return mutations.size();
    }

    @Override
    public Object getItem(int pos) {
        return mutations.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return 0;//list.get(pos).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.custom_listview, null);
        }
        TextView zmiana = (TextView) view.findViewById(R.id.zmiana);
        TextView name = (TextView) view.findViewById(R.id.name);
        ImageView mark = view.findViewById(R.id.mark);
        name.setText(mutations.get(position).getName());
        if(Objects.equals(mutations.get(position).getName(), "Deletion")){
            zmiana.setText(MessageFormat.format("{0}->OUT", mutations.get(position).getBefore()));
        }
        else if(Objects.equals(mutations.get(position).getName(), "Insertion")){
            zmiana.setText(MessageFormat.format("{0}->IN", mutations.get(position).getAfter()));
        }
        else if(Objects.equals(mutations.get(position).getName(), "Substitution")){
            zmiana.setText(MessageFormat.format("{1}->{0}", mutations.get(position).getBefore(), mutations.get(position).getAfter()));
            if(Objects.equals(mutations.get(position).getPosition(), "99") && Objects.equals(mutations.get(position).getAfter(), "V") && Objects.equals(mutations.get(position).getBefore(), "E"))
                mark.setImageResource(R.drawable.anemia);
        }
        TextView pozycja = (TextView) view.findViewById(R.id.pozycja);
        pozycja.setText(MessageFormat.format("Position: {0}", mutations.get(position).getPosition()));

        return view;
    }

}