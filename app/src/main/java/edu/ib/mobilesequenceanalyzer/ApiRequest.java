package edu.ib.mobilesequenceanalyzer;

import android.content.Intent;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.atomic.AtomicReference;


public class ApiRequest {

    private String sekwencja = null;

    public String getRequest(String concatData) throws Exception {
        //AtomicReference<String> sekwencja = null;
        Thread thread = new Thread(() -> {
            String requestURL = "https://www.ebi.ac.uk/proteins/api/proteins/" + concatData;
            URL url = null;

            try {
                url = new URL(requestURL);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }


            URLConnection connection = null;

            try {
                connection = url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }

            HttpURLConnection httpConnection = (HttpURLConnection) connection;

            httpConnection.setRequestProperty("Accept", "application/json");

            InputStream response = null;
            try {
                response = connection.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

            int responseCode = 0;
            try {
                responseCode = httpConnection.getResponseCode();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (responseCode != 200) {
                throw new RuntimeException("Response code was not 200. Detected response was " + responseCode);
            }

            String output;
            Reader reader = null;
            try {
                try {
                    reader = new BufferedReader(new InputStreamReader(response, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                StringBuilder builder = new StringBuilder();
                char[] buffer = new char[8192];
                int read = 0;
                while (true) {
                    try {
                        if (!((read = reader.read(buffer, 0, buffer.length)) > 0)) break;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    builder.append(buffer, 0, read);
                }
                output = builder.toString();
            } finally {
                if (reader != null) try {
                    reader.close();
                } catch (IOException logOrIgnore) {
                    logOrIgnore.printStackTrace();
                }
            }
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            System.out.println(output);

            JsonObject results = gson.fromJson(output.toString(), JsonObject.class);

            JsonObject jakos = results.getAsJsonObject("sequence");
            System.out.println(jakos.toString());
            JsonPrimitive sekw = jakos.get("sequence").getAsJsonPrimitive();
            System.out.println(sekw.getAsString());
            sekwencja = (sekw.getAsString());
        });
        thread.start();
        return sekwencja;
    }
}